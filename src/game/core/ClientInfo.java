package game.core;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;


public class ClientInfo {
	private BufferedWriter clientWriter;
	private BufferedReader clientReader;
	private PlayerInfo playerRef;
	private Socket clientSock;
	//public byte[] clientID;
	
	
	public ClientInfo(Socket s, PlayerInfo ref) {
		clientSock = s;
		try {
			clientWriter = new BufferedWriter(
					new OutputStreamWriter(clientSock.getOutputStream()));
			clientReader = new BufferedReader(
					new InputStreamReader(clientSock.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		playerRef = ref;
	}
	
	public int getID() {
		return playerRef.playerID;
	}
	
	public void setID(int id) {
		playerRef.playerID = id;
	}
	
	public BufferedWriter getClientWriter() {
		return clientWriter;
	}

	public void setClientWriter(BufferedWriter clientWriter) {
		this.clientWriter = clientWriter;
	}

	public PlayerInfo getPlayerRef() {
		return playerRef;
	}

	public void setPlayerRef(PlayerInfo playerRef) {
		this.playerRef = playerRef;
	}

	public Socket getClientSock() {
		return clientSock;
	}

	public void setClientSock(Socket clientSock) {
		this.clientSock = clientSock;
	}

	public ClientInfo(Socket s, byte[] id) {
		clientSock = s;
		//clientID = id;
	}

	
	public void setClientReader(BufferedReader clientReader) {
		this.clientReader = clientReader;
	}
	

	public BufferedReader getClientReader() {
		return clientReader;
	}
	
	/*
	public boolean equals(Object other) {
		Client c = (Client) other;
		if(c.clientID == this.clientID) return true;
		return false;
	}*/
}
