package game.core;


import game.misc.Command;
import game.misc.GameSettings;
import game.misc.GameUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MasterServer {
	ArrayList<RoomInfo> registeredRooms;
	ServerSocket masterSock, clientHandle;
	Connection dbConnection;
	
	public Logger MASTERLOG = Logger.getLogger("Master Server Logger");
	
	public static void main(String[] args) {
		new MasterServer().startServer();
	}
	
	public MasterServer() {
		registeredRooms = new ArrayList<RoomInfo>();
		try {
			masterSock = new ServerSocket(GameSettings.MASTER_ROOM_PORT);
			clientHandle = new ServerSocket(GameSettings.MASTER_CLIENT_PORT);
			MASTERLOG.log(Level.INFO, 
					"MASTER SERVER STARTED ON " + masterSock.getLocalPort());
			MASTERLOG.log(Level.INFO, 
					"CLIENT HANDLE STARTED ON " + clientHandle.getLocalPort());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void startServer() {
		if(!GameSettings.DEBUG_MODE) {
			dbConnection = 
				connectToDB("jdbc:mysql://localhost:3306/boxuz_db", "root", "Ajay1123");
		}
		          
		new Thread(new RoomAcceptor()).start();
		new Thread(new ClientAcceptor()).start();
	}
	
	public Connection connectToDB(String db_connect_string,
	          String db_userid, String db_password)   {
	                try {
	                        Class.forName("com.mysql.jdbc.Driver").newInstance();
	                        Connection conn = DriverManager.getConnection(
	                          db_connect_string, db_userid, db_password);
	      
	                        System.out.println("connected");
	                        return conn;  
	                }
	                catch (Exception e){
	                        e.printStackTrace();
	                        return null;
	                }
	       }

	public boolean isValidCredential(String suppliedUname, String suppliedPass) {
		try {
			Statement getCreds = (Statement) dbConnection.createStatement();
			ResultSet rs = getCreds.executeQuery("SELECT username, password FROM accounts");
			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				System.out.println(username + " " + password);
				if(username.equals(suppliedUname)
				    && password.equals(suppliedPass)) {
					System.out.println("Login successfull with: " 
							+ suppliedUname + " " + suppliedPass);
					return true;
				} 
				/*else {
					    System.out.println("[LOGIN] Failed login @: " + clientSocket.getInetAddress());
					    System.out.println("[LOGIN] Client has been kicked from login.");
					    printWrite.write("@KICK\n");
					    printWrite.flush();
					    clientSocket.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Login failed with: " 
				+ suppliedUname + " " + suppliedPass);
		return false;
	}
	
	class ClientAcceptor implements Runnable {
		@Override
		public void run() {
			Socket currClient = null;
			try {
				while(true) {
					currClient = clientHandle.accept();
					new Thread(new ClientHandler(currClient)).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
		
	}
		
	class ClientHandler implements Runnable {
		Socket currClient = null;
		BufferedWriter clientWriter = null;
		BufferedReader clientReader = null;
		
		public ClientHandler(Socket cli) {
			currClient = cli;
			try {
				clientWriter = new BufferedWriter(new OutputStreamWriter(
						currClient.getOutputStream()));
				clientReader = new BufferedReader(
						new InputStreamReader(currClient.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}
		
		@Override
		public void run() {
				String input = null;
				try {
				while((input = clientReader.readLine()) != null) {
						if(input.startsWith(Command.REQ_ROOMS)) {
							System.out.println("ROOMS REQUESTED");
							InetSocketAddress roomAddress;
							for(RoomInfo currRoom: registeredRooms) {
								roomAddress = currRoom.getRoomAddress();
								System.out.println(roomAddress.getHostName() + ","
									+ roomAddress.getPort());
								String roomInfo = Command.IN_LIST + "!!"
									+ currRoom.getRoomID() + ","
									+ roomAddress.getHostName() + ","
									+ roomAddress.getPort() + ","
									+ currRoom.getRoomName() + ","
									+ currRoom.getMapName();
								GameUtils.writeLine(clientWriter, 
										roomInfo);
							}
							GameUtils.writeLine(clientWriter, 
									Command.END_OF_LIST);
						} else if(input.startsWith(Command.LOGIN_CRED)) {
							String[] loginData = input.split("!")[2].split(",");
							if(isValidCredential(loginData[0], 
									loginData[1])) {
								GameUtils.writeLine(clientWriter, 
										Command.LOGIN_SUCC);
							} else {
								GameUtils.writeLine(clientWriter, 
										Command.LOGIN_FAIL);
							}
						}	
				}
			} catch (IOException e) {
				System.out.println("CLIENT EXITED");
				try {
					currClient.close();
					clientWriter.close();
					clientReader.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
					
			}	
		}
		
	}
	
	public void closeClientConnection() {
		
	}
	class RoomAcceptor implements Runnable {
		Socket newRoomSock = null;
		RoomInfo newRoom;
		BufferedReader newRoomReader;
		BufferedWriter newRoomWriter;
		@Override
		public void run() {
			while(true) {
				try {
					newRoomSock = masterSock.accept();
					newRoom = new RoomInfo(
							newRoomSock);
					newRoomReader = newRoom.getRoomReader();
					newRoomWriter = newRoom.getRoomWriter();
				} catch (IOException e) {
					e.printStackTrace();
				}
				String input;
				String[] roomData;
				try {
					if((input = newRoomReader.readLine()) != null) {
						if(input.startsWith(Command.ADD_ROOM)) {
								if(registeredRooms.size() < GameSettings.MAX_ROOMS) {
									roomData = input.split("!")[2].split(",");
									int roomID = (int)(Math.random()*10000)+1;
									newRoom.setRoomID(roomID);
									newRoom.setRoomAddress(
											new InetSocketAddress(roomData[0], 
													Integer.parseInt(roomData[1])));
									newRoom.setRoomName(roomData[2]);
									newRoom.setMapName(roomData[3]);
									registeredRooms.add(newRoom);
									GameUtils.writeLine(newRoomWriter, 
											Command.ROOM_ADDED + "!!" + roomID);
									new Thread(new RoomHandler(newRoom)).start();
									System.out.println("NEW ROOM");
								} else {
									GameUtils.writeLine(newRoomWriter, 
											Command.ROOM_REJECTED);
									try {
										newRoom.getRoomSock().close();
										newRoom.getRoomWriter().close();
										newRoom.getRoomReader().close();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	class RoomHandler implements Runnable {
		private RoomInfo thisRoom;
		
		public RoomHandler(RoomInfo room) {
			thisRoom = room;
		}
		
		@Override
		public void run() {
			String input;
			try {
				while((input = thisRoom.getRoomReader().readLine()) != null) {
						
				}
			} catch (IOException e) {
				try {
					thisRoom.getRoomSock().close();
					thisRoom.getRoomWriter().close();
					thisRoom.getRoomReader().close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				registeredRooms.remove(thisRoom);
				System.out.println("ROOM CLOSED: " + thisRoom.getRoomID());
				thisRoom = null;
				e.printStackTrace();
			}
		}
	}
	
}
