package game.core;

public class PlayerInfo {
	public int x, y, health, speed, playerID;
	
	public PlayerInfo() {
		x = 10;
		y = 10;
		health = 100;
		speed = 3;
		playerID = -1;
	}
	
	public PlayerInfo(int x, int y, int health, int speed, int id) {
		this.x = x;
		this.y = y;
		this.health = health;
		this.speed = speed;
		playerID = id;
	}
	
}
