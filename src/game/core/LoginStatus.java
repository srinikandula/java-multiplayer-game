package game.core;

public class LoginStatus {
	private String username;
	private String pass;
	private boolean loginSuccess;
	
	public LoginStatus() {
		loginSuccess = false;
	}
	
	public void setLoginSuccess(boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}
	
	public boolean isLoginSuccess() {
		return loginSuccess;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPass() {
		return pass;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
	
	public void clearData() {
		username = "";
		pass = "";
		loginSuccess = false;
	}
}