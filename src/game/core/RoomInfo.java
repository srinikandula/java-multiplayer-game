package game.core;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;


public class RoomInfo {
	private BufferedWriter roomWriter;
	private BufferedReader roomReader;
	private Socket roomSock;
	private int roomID;
	private InetSocketAddress roomServerAddress;  
	private InetSocketAddress roomAddress;
	private String mapName, roomName;
	
	public RoomInfo(Socket sock) throws IOException {
		roomSock = sock;
		roomWriter = new BufferedWriter(
					new OutputStreamWriter(sock.getOutputStream()));
		setRoomReader(new BufferedReader(
				new InputStreamReader(sock.getInputStream())));
		roomServerAddress = new InetSocketAddress(
				roomSock.getInetAddress(),
				roomSock.getPort());
	}
	
	public BufferedWriter getRoomWriter() {
		return roomWriter;
	}

	public void setRoomWriter(BufferedWriter roomWriter) {
		this.roomWriter = roomWriter;
	}

	public Socket getRoomSock() {
		return roomSock;
	}

	public void setRoomSock(Socket roomSock) {
		this.roomSock = roomSock;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public InetSocketAddress getRoomAddress() {
		return roomAddress;
	}

	public void setRoomAddress(InetSocketAddress roomAddress) {
		this.roomAddress = roomAddress;
	}

	
	public void setRoomReader(BufferedReader roomReader) {
		this.roomReader = roomReader;
	}
	

	public BufferedReader getRoomReader() {
		return roomReader;
	}
	
	public InetSocketAddress getRoomServerAddress() {
		return roomServerAddress;
	}

	public void setRoomServerAddress(InetSocketAddress roomServerAddress) {
		this.roomServerAddress = roomServerAddress;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getMapName() {
		return mapName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomName() {
		return roomName;
	}

}
