package game.core;


import game.entities.GameMap;
import game.misc.Command;
import game.misc.GameSettings;
import game.misc.GameUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Room {
	private ArrayList<ClientInfo> clients;
	private Socket masterConnector;
	private ServerSocket roomSock;
	private BufferedWriter masterWriter;
	private BufferedReader masterReader;
	private HashMap<Integer, ClientInfo> ids;
	private Logger ROOMLOG = Logger.getLogger("Room Logger");
	private int roomID, maxPlayers;
	private String hostName, mapName, roomName; 
	
	public static void main(String args[]) {
		new Room("DefaultRoom", GameSettings.MAX_PLAYERS).setUpRoom();
	}
	
	private Room(String name, int maxP) {
		maxPlayers = maxP;
		roomName = name;
		mapName = "Barren Wasteland";
		clients = new ArrayList<ClientInfo>();
		ids = new HashMap<Integer, ClientInfo>(maxPlayers);
	}
	
	private void setUpRoom() {
		try {
			hostName = InetAddress.getLocalHost().getHostName();
			int gamePort = GameSettings.GAME_ROOM_PORT;
			try {
				roomSock = new ServerSocket(gamePort);
			} catch (BindException e) {
				System.out.println("CHANGING PORT...");
				gamePort = GameUtils.getAvailiblePort(GameSettings.GAME_ROOM_PORT, 
						GameSettings.GAME_ROOM_PORT+100);
				System.out.println("PORT CHANGED TO: " + gamePort);
				try {
					roomSock = new ServerSocket(gamePort);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			masterConnector = new Socket(
					GameSettings.MASTER_SERVER,
					GameSettings.MASTER_ROOM_PORT);
			masterWriter = new BufferedWriter(
					new OutputStreamWriter(masterConnector.getOutputStream()));
			masterReader = new BufferedReader(
					new InputStreamReader(masterConnector.getInputStream()));
			GameUtils.writeLine(masterWriter, Command.ADD_ROOM + "!!"
					+ hostName + "," 
					+ roomSock.getLocalPort() + ","
					+ roomName + ","
					+ mapName);
			String reply;
			if((reply = masterReader.readLine()) != null) {
				if(reply.startsWith(Command.ROOM_ADDED)) {
					roomID = Integer.parseInt(reply.split("!")[2]);
					System.out.println("SUCCESSFULLY REGISTERED " +
							"WITH MASTER: " + roomID);		
					new Thread(new MasterListener()).start();
					new Thread(new ClientAcceptor()).start();
				} else if(reply.startsWith(Command.ROOM_REJECTED)) {
					System.out.println("ROOM REGISTRATION FAILIURE.");
					System.exit(0);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	class ClientAcceptor implements Runnable {
		Socket currSock;
		public ClientAcceptor() {	
			ROOMLOG.log(Level.INFO, "SERVER STARTED ON " + roomSock.getLocalPort());
		}
		@Override
		public void run() {
			ClientInfo currClient;
			while(true) {
				try {
					currSock = roomSock.accept();
					currClient = new ClientInfo(currSock, new PlayerInfo());			
					clients.add(currClient);
					new Thread(new ClientHandler(currClient)).start();
					ROOMLOG.log(Level.INFO, "CLIENT ACCEPTED: " + 
							currSock.getInetAddress());
					System.out.println("CLIENT COUNT: " + clients.size());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	class ClientHandler implements Runnable {
		BufferedReader thisClientReader;
		BufferedWriter thisClientWriter;
		ClientInfo thisClient;
		PlayerInfo thisPlayer;
		
		public ClientHandler(ClientInfo cli) {
			thisClient = cli;
			thisClientReader = thisClient.getClientReader();
			thisClientWriter = thisClient.getClientWriter();
			thisPlayer = thisClient.getPlayerRef();
		}
		
		@Override
		public void run() {
			String incoming = null;
				try {
					while((incoming = thisClientReader.readLine()) != null) {
							handleInput(incoming, thisClientWriter, thisClient);
					}
				} catch (Exception e) {
					ROOMLOG.log(Level.INFO, "CLIENT EXITED: " + 
							thisClient.getClientSock().getInetAddress() 
							+ " with ID "+ thisClient.getID());
					/*ROOMLOG.log(Level.INFO, "COMMAND: " + 
							incoming);*/
					clients.remove(thisClient);
					ids.remove(thisClient.getID());
					try {
						thisClient.getClientSock().close();
						thisClientWriter.close();
						thisClientReader.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					thisClient = null;
					return;
				}
		}		
	
		public void handleInput(String input, BufferedWriter replyToClient, ClientInfo thisClient) {
			String[] inputData = input.split("!");
			String command = inputData[0];
			String[] info = inputData[2].split(",");
			if(command.equals(Command.REQ_ENTRY)) {
				if(clients.size() < GameSettings.MAX_PLAYERS) {
					int id = (int)(Math.random()*10000) + 1;
					
					thisPlayer = new PlayerInfo(
							Integer.parseInt(info[0]), 
							Integer.parseInt(info[1]), 
							Integer.parseInt(info[2]),
							Integer.parseInt(info[3]), id);
					GameUtils.writeLine(
							replyToClient, Command.ROOM_ENTERED + "!" 
											+ id + "!" + id);
					thisClient.setPlayerRef(thisPlayer);
					ids.put(id, thisClient);
					String clientInfo = "";
					int i = 0;
					for(ClientInfo currClient: clients) {
						System.out.println("ID " + i + ":" + currClient.getID());
						i++;
					}
					PlayerInfo currPlayer;
					for(ClientInfo currClient: clients) {
						if(currClient.getID() == thisClient.getID()) continue;
						currPlayer = currClient.getPlayerRef();
						clientInfo = Command.EXISTING + "!" + currClient.getID()
									+ "!" + currPlayer.x + ","
									+ currPlayer.y + ","
									+ currPlayer.health + ","
									+ currPlayer.speed;

						GameUtils.writeLine(replyToClient, clientInfo);
					}
					clientInfo = "";
					clientInfo = Command.NEWCOMER + "!" + thisPlayer.playerID
					+ "!" + thisPlayer.x + ","
					+ thisPlayer.y + ","
					+ thisPlayer.health + ","
					+ thisPlayer.speed;
					
					relayInformation(clientInfo);
					
				} else {
					GameUtils.writeLine(replyToClient, 
							Command.SERV_LIM_REACHED + "!!" + clients.size());
				}
			} else if(command.equals(Command.REQ_EXIT)) {
				ids.remove(Integer.parseInt(inputData[2]));
				clients.remove(thisClient);
				relayInformation(Command.PLAYER_EXITED + "!!" + inputData[2]);
				return;
			} else {
				int playerID = Integer.parseInt(inputData[1]);
				if(command.equals(Command.INFO)) {
					relayInformation(input);
				} else if(command.equals(Command.SYNC_INFO)) {
					PlayerInfo pToUpdate = ids.get(playerID).getPlayerRef();
					pToUpdate.x = Integer.parseInt(info[0]);
					pToUpdate.y = Integer.parseInt(info[1]);
					relayInformation(input);
				} else relayInformation(input);	
			}
		}
	}
	
	class MasterListener implements Runnable {
		@Override
		public void run() {
			String reply;
			try {
				while((reply = masterReader.readLine()) != null) {	
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void relayInformation(String info) {
		for(ClientInfo currClient: clients) {
			GameUtils.writeLine(currClient.getClientWriter(), info);
		}
	}

	
}
