package game.misc;


public class Command {
		public static final String PLAYER_EXITED = "pExited";
		public static final String NEWCOMER = "newP";
		public static final String EXISTING = "existP";
		public static final String INFO = "inf";
		public static final String SYNC_INFO = "syncInf";
		public static final String ROOM_ENTERED = "rmEntrd";
		//public static final String READY_FOR_INFO = "readyForInfo";
		public static final String REQ_ENTRY = "reqEntry";
		public static final String REQ_EXIT = "reqExit";
		public static final String REQ_ROOMS = "reqRmLst";
		public static final String REQ_ROOM_COUNT = "reqRmCnt";
		public static final String ADD_ROOM = "addRm";
		public static final String ROOM_ADDED = "rmAdded";
		public static final String ROOM_REJECTED = "rmRej";
		public static final String IN_LIST = "availRm";
		public static final String END_OF_LIST = "endRmLst";
		public static final String SERV_LIM_REACHED = "servLimMax";
		public static final String WEP_FIRE = "wepFire";
		public static final String LOGIN_CRED = "login";
		public static final String LOGIN_SUCC = "loginSucc";
		public static final String LOGIN_FAIL = "loginFail";
}
