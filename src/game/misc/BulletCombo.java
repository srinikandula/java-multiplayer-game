package game.misc;
import game.entities.Bullet;
import game.entities.Player;
import static game.entities.Weapon.WeaponType;

import java.awt.Color;
import java.awt.Point;
import java.util.concurrent.CopyOnWriteArrayList;



public class BulletCombo {

	public static void spawnShot(WeaponType weapon, 
			CopyOnWriteArrayList<Bullet> bullets, 
			Player parent, Point approxDest) {
		float deltaX = parent.center.x - approxDest.x;
		float deltaY = parent.center.y - approxDest.y;
		float distFromDest = (float) Math.sqrt(deltaX*deltaX+deltaY*deltaY);
		float origAngle = (float) 
		Math.toRadians(((180.0f-Math.toDegrees(Math.atan2(deltaY, deltaX)))+360f)%360f);
		double offsetTheta, newX, newY;
		
		switch(weapon) {
		case PISTOL:
			bullets.add(
					new Bullet(parent.x, parent.y,
					weapon.shotSize, weapon.shotSize, Color.CYAN, approxDest.x,
					approxDest.y,  weapon.shotSpeed, parent));
		
			break;
		case SHOTGUN:
			offsetTheta = Math.toRadians(20);
			//System.out.println("DIST FROM DEST: " + distFromDest);
			for(int i = -1; i <= 1; i++) {
				newX = (distFromDest * Math.cos(origAngle + offsetTheta*i));
				newY =  -(distFromDest * Math.sin(origAngle + offsetTheta*i));
				//System.out.print("X: " + x + " Y: " + y);
				//System.out.println("DIST FROM DEST: " + distFromDest);
				bullets.add(
						new Bullet(parent.x, parent.y,
						weapon.shotSize, weapon.shotSize, Color.RED,(int) 
						newX + parent.x,
						(int)newY + parent.y, weapon.shotSpeed, parent));
			}
			break;
		case FRAG_GRENADE:
			offsetTheta = Math.toRadians(15);
			for(int i = 0; i < 24; i++) {
				newX = (distFromDest * Math.cos(origAngle + offsetTheta*i));
				newY =  (distFromDest * Math.sin(origAngle + offsetTheta*i));
				bullets.add(
						new Bullet(parent.x, parent.y,
						weapon.shotSize, weapon.shotSize, Color.BLUE, 
						(int)newX + parent.x,
						(int)newY + parent.y, weapon.shotSpeed,  parent));
			}
			
			break;
		}
	}
}
