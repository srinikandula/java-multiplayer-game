package game.misc;


import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;


public class GameUtils {
	static {
		try {
			//ROOMLOG.addHandler(new FileHandler("ServerLog.txt"));
			//CLILOG.addHandler(new FileHandler("ClientLog.txt"));
			//CLILOG.addHandler(new FileHandler("ClientLog.txt"));
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public static void writeLine(BufferedWriter writer, String data) {
		try {
			writer.write(data);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			//CLILOG.log(Level.SEVERE, "Error writing: " + data);
			//ROOMLOG.log(Level.SEVERE, "Error writing: " + data);
			e.printStackTrace();
		}
	}
	
	public static int getAvailiblePort(int minPort, int maxPort) {
		boolean availible = false;
	    int port = minPort;
		if (port < minPort || port > maxPort) {
	        throw new IllegalArgumentException("Invalid port range/ports: " + port);
	    }   
	    do {
		    ServerSocket ss = null;
		    //DatagramSocket ds = null;
		    try {
		        ss = new ServerSocket(port);
		        ss.setReuseAddress(true);
		       //ds = new DatagramSocket(port);
		        //ds.setReuseAddress(true);
		        return port;
		    } catch (IOException e) {
		    } finally {
		        /*if (ds != null) {
		            ds.close();
		        }*/
		        if (ss != null) {
		            try {
		                ss.close();
		            } catch (IOException e) {
		            }
		        }
		    }
		    port++;
		} while(!availible);
	    return -1;
	}
	
	public static boolean bufferedImageEquals( BufferedImage b1, BufferedImage b2 ) {
	    if ( b1 == b2 ) {return true;} // true if both are null
	    if ( b1 == null || b2 == null ) { return false; }
	    if ( b1.getWidth() != b2.getWidth() ) { return false; }
	    if ( b1.getHeight() != b2.getHeight() ) { return false; }
	    for ( int i = 0; i < b1.getWidth(); i++) {
	     for ( int j = 0; j < b1.getHeight(); j++ ) {
	       if ( b1.getRGB(i,j) != b2.getRGB(i,j) ) { 
	           return false;
	       }
	      }
	    }
	    return true;
	  }
	
}
