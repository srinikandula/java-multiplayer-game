package game.misc;


import java.net.InetAddress;
import java.net.UnknownHostException;


public class GameSettings {
	public static InetAddress MASTER_SERVER = null;
	public static final int MAX_PLAYERS = 20;
	public static final int MAX_ROOMS = 20;
	public static final int MASTER_ROOM_PORT = 55555;
	public static final int MASTER_CLIENT_PORT = 44444;
	public static final int GAME_ROOM_PORT = 33333;
	public static int THREAD_IO_DELAY = 45;
	public static int GAME_DELAY = 15;
	public static int PLAYER_SIZE = 15;
	public static int BLOCK_SIZE = 25;
	public static final boolean DEBUG_MODE = true;
	
	static {
		try {
			MASTER_SERVER = InetAddress.getByName("ssdeath-ownz");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
