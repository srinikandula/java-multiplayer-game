package game.entities;
import java.awt.Point;
import java.awt.Rectangle;

public abstract class GameCharacter extends GameObject {
	public Point topRight, topLeft, 
	bottomRight, bottomLeft;
	int incr = 5;
	
	public GameCharacter(int x, int y, int w, int l) {
		super(x, y, w, l);
		topRight = new Point(x+width+incr,y-incr);
		topLeft = new Point(x-incr,y-incr);
		bottomLeft = new Point(x-incr,y+height+incr);
		bottomRight = new Point(x+width+incr,y+height+incr);
	}
	
	public void setPoints() {
		center.setLocation(x+width/2,y+height/2);
		topRight.setLocation(x+width+incr,y-incr);
		topLeft.setLocation(x-incr,y-incr);
		bottomLeft.setLocation(x-incr,y+height+incr);
		bottomRight.setLocation(x+width+incr,y+height+incr);	
	}
}