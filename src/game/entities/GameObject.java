package game.entities;
import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;


public abstract class GameObject extends Rectangle {
	public Image image;
	public Color color;
	public Point center;
	
	public GameObject(int x, int y, int width, int height) {
		super(x, y, width, height);
		center = new Point(x+width/2,y+height/2);
	}
	public abstract void setPoints();
}
