package game.entities;


public abstract class Weapon {
	private int ammo;
	
	public enum WeaponType {
		PISTOL(0, 10, 6, 4), 
		SHOTGUN(2, 8, 5, 6), 
		FRAG_GRENADE(9, 7, 9, 3),
		INVALID_WEAPON(-1, -1, -1, -1);
		
		public int getShotSpeed() {
			return shotSpeed;
		}

		public void setShotSpeed(int shotSpeed) {
			this.shotSpeed = shotSpeed;
		}

		public int getShotDamage() {
			return shotDamage;
		}

		public void setShotDamage(int shotDamage) {
			this.shotDamage = shotDamage;
		}

		public int weaponID, shotSize, shotSpeed, shotDamage;
		WeaponType(int id, int size, int speed, int damage) {
			weaponID = id;
			shotSize = size;
			shotSpeed = speed;
			shotDamage = damage;
		}

		public int getWeaponID() {
			return weaponID;
		}
			
		public static WeaponType getWeapon(int weaponID) {
			for(WeaponType wep: values()) {
				if(wep.weaponID == weaponID) return wep;
			}
			return INVALID_WEAPON;
		}
	}
	
	public abstract void fire();
	
	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}
	
	public int getAmmo() {
		return ammo;
	}
	
}
