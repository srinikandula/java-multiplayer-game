package game.entities;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class Terrain extends GameObject {
	private boolean isSpawn;
	private static Color DIRT_C = new Color(244,164,96);
	private static Color GRASS_C = new Color(34,139,34);
	private static Color WATER_C = new Color(22,168,252);
	private static Color MUD_C = new Color(139,69,19);
	private static Color ROAD_C = new Color(119,136,153);
	private static Color WALL_C = Color.ORANGE;
	
	// enum ID is to differentiate Tile types
	public enum TerrainType {	
		DIRT(DIRT_C, '0'), 
		GRASS(GRASS_C, '1'), 
		WATER(WATER_C, '2'),
		MUD(MUD_C, '3'), 
		ROAD(ROAD_C, '4'),
		WALL(WALL_C, '5'),
		ERROR(Color.RED, '\0');
	
		public BufferedImage image = null;
		public Color color; 
		public char code;
		
		TerrainType(Color c, char cd) {
			color = c;
			code = cd;
		}
		
		TerrainType(BufferedImage img, char cd) {
			image = img;
			code = cd;
		}
	
		public static TerrainType getType(char code) {
			for(TerrainType t: values()) {
				if(t.code == code) return t;
			}
			return TerrainType.ERROR;
		}
		
		public static TerrainType getType(Color c) {
			for(TerrainType t: values()) {
				if(t.color == c) return t;
			}
			return TerrainType.ERROR;
		}
		
		public static TerrainType getType(Image img) {
			for(TerrainType t: values()) {
				if(t.image.equals(img)) return t;
			}
			return TerrainType.ERROR;
		}
	}
	
	private TerrainType type;
	
	public TerrainType getType() {
		return type;
	}

	public Terrain(int x, int y, int width, int height, TerrainType type) {
		super(x, y, width, height);
		image = null;
		color = type.color;
		image = type.image;
		this.type = type;
		setSpawn(false);
	}

	@Override
	public void setPoints() {
		
	}

	
	public void setSpawn(boolean isSpawn) {
		this.isSpawn = isSpawn;
	}
	

	public boolean isSpawn() {
		return isSpawn;
	}
	
}