package game.entities;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Ellipse2D;


public class Bullet extends GameObject {
	public int bullet_life, damage, bulletSpeed;
	private int mag;
	private float deltaX, deltaY;
	Player parent;
	
	public Bullet(int x, int y, int width, int height, Color color, 
				int destX, int destY, int speed, Player parent) {
		super(x, y, width, height);
		this.color = color;
		center = new Point(x+width/2,y+height/2);
		deltaX = destX - center.x;
		deltaY = destY - center.y;
		this.parent = parent;
		bullet_life = 75;
		damage = 4;
		bulletSpeed = speed;
	}
	
	public void move() {
		setPoints();
		mag =  (int) Math.sqrt( deltaX*deltaX + deltaY*deltaY );
		deltaX = deltaX / mag * bulletSpeed;
		deltaY = deltaY / mag * bulletSpeed;
        x+=deltaX;
        y+=deltaY;
        bullet_life--;
	}
	
	public void setPoints() {
		center.setLocation(x+width/2,y+height/2);
	}
	
	public int getBullet_life() {
		return bullet_life;
	}

	public void setBullet_life(int bullet_life) {
		this.bullet_life = bullet_life;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getBulletSpeed() {
		return bulletSpeed;
	}

	public void setBulletSpeed(int bulletSpeed) {
		this.bulletSpeed = bulletSpeed;
	}

	public Player getParent() {
		return parent;
	}


}
