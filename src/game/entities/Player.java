package game.entities;

import static game.entities.Weapon.WeaponType;

import java.awt.Color;

public class Player extends GameCharacter {
	private int playerID;
	public int health, speed, killCount;
	private WeaponType currWeapon;
	public boolean movUp, movDown, movLeft, movRight,
					movUpBlocked, movDownBlocked, 
					movLeftBlocked, movRightBlocked,
					onGround;
	
 	public Player(int x, int y, int width, int height, Color c) {
		super(x,y,width,height);
		color = c;
		playerID = 0;
		health = 100;
		killCount = 0;
		currWeapon = WeaponType.FRAG_GRENADE;
	}
	
	public Player(int x, int y, int width, int height, 
			int id, int health, int speed, Color c) {
		super(x,y,width,height);
		color = c;
		playerID = id;
		this.health = health;
		this.speed = speed;
		killCount = 0;
		currWeapon = WeaponType.FRAG_GRENADE;
	}
	
	public boolean equals(Object other) {
		if(this.playerID == ((Player) other).playerID) return true;
		return false;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getPlayerID() {
		return playerID;
	}

	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public WeaponType getCurrWeapon() {
		return currWeapon;
	}

	public void setCurrWeapon(WeaponType currWeapon) {
		this.currWeapon = currWeapon;
	}

	public int getHealth() {
		return health;
	}
	
	public int getKillCount() {
		return killCount;
	}
}
