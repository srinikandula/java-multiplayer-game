package game.entities;

import java.util.HashMap;

public class GameMap {
	private static final HashMap<String, String> GAME_MAPS;
	
	static {
		GAME_MAPS = new HashMap<String, String>();
		GAME_MAPS.put("Barren Wasteland", "map1.txt");
	}
	public static String getMapByName(String mapAlias) {
		return GAME_MAPS.get(mapAlias);
	}
	
}
